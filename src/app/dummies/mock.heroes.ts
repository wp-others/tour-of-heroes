import { Hero } from '../models/hero';

export const HEROES :Hero[] = [
  { id: 1, name: "Roni" },
  { id: 2, name: "Carlos" },
  { id: 3, name: "Alvin" },
  { id: 4, name: "Licona" },
];
